// Copyright (C) 2014 John Papa
//
// This file contains substantial parts from `johnpapa/pluralsight-gulp`,
// released under the MIT License (MIT).
//
// `chavz/angular-starter-kit` is distributed under
// the MIT License (MIT), for more info, see LICENSE.

module.exports = function() {
  var root = './';
  var src = './src/';
  var app = src + 'app/';
  var build = './build/';
  var temp = './.tmp/';
  var report = './report/';

  var specRunnerFile = 'SpecRunner.html';

  var wiredep = require('wiredep');
  var bowerFiles = wiredep({devDependencies: true})['js'];

  var appDefaultConfigFolder = 'configurations/';
  var applicationConfigFile = 'appconf';

  var config = {
    src: src,
    app: app,
    root: root,
    temp: temp,
    build: build,
    report: report,

    /**
     * App Config Locations
     */
    appConf: {
      filename: applicationConfigFile,
      jsonFile: root + applicationConfigFile.concat('.json'),
      jsFile: temp + applicationConfigFile.concat('.js'),
      module: 'configurations',
      defaultConfig: {
        folder: root + appDefaultConfigFolder,
        file: root + appDefaultConfigFolder + 'common.json',
        environment: 'development'
      },
      environments: ['development', 'production', 'presentation']
    },

    /**
     * HTML Locations
     */
    allHtml: src + '**/*.html',
    indexHtml: src + 'index.html',
    htmltemplates: [
      app + '**/*.html',
      app + '**/*.tpl'
    ],

    /**
     * Scripts Locations
     */
    allScripts: [
      './src/**/*.js',
      './*.js'
    ],
    appScripts: [
      app + '**/*.module.js',         // load all modules first
      app + '**/*.js',                // then the rest of the js files
      '!' + app + '**/*.spec.js',     // but exclude all tests
      '!' + app + '**/*.mockdata.js', // as well as mock datas
      '!' + app + '**/*.mockhttp.js'  // and http mocks
    ],
    appScriptsOrder: [
      '**/app.module.js',
      '**/*.module.js',
      '**/*.js'
    ],

    /**
     * Styles Locations
     */
    appStyles: src + 'styles/**/*.css',
    processedStyles: temp + './main.css',

    /**
     * Fonts Locations
     */
    appFonts: [
      src + 'fonts/**/*.*',
      './bower_components/bootstrap/fonts/**/*.*',
      './bower_components/font-awesome/fonts/**/*.*'
    ],

    /**
     * Images Locations
     */
    appImages: src + 'images/**/*.*',

    /**
     * Bower and NPM Locations
     */
    bower: {
      json: require('./bower.json'),
      directory: './bower_components',
      ignorePath: '../',
      exclude: [/bootstrap\.js/]
    },
    npm: {
      json: require('./package.json'),
      directory: './node_modules'
    },
    packages: [
      './package.json',
      './bower.json'
    ],

    /**
     * JsDoc Locations
     */
    jsDoc: {
      exe: './node_modules/jsdoc/jsdoc.js',
      dest: build + 'docs',
      conf: {
        files: [
          root + 'jsdoc.conf.json',   // our very own config file
          './node_modules/angular-jsdoc/common/conf.json'  // angular-jsdoc config file
        ],
        order: [
          '**/conf.json',       // load all 3rd party plugins conf first
          '**/jsdoc.conf.json'  // then our own conf file (last)
        ]
      },
      template: './node_modules/angular-jsdoc/default'
    },

    /**
     * Template Cache
     */
    templateCache: {
      file: 'templates.js',
      options: {
        root: 'app/',
        module: 'app.core',
        standAlone: false,
      }
    },

    /**
     * Optimized files
     */
    optimized: {
      app: 'app.js',
      lib: 'lib.js'
    },

    /**
     * SpecRunner.html, our HTML spec runner
     */
    specRunner: src + specRunnerFile,
    specRunnerFile: specRunnerFile,

    /**
     * The sequence of the injections into specs.html:
     *  1 testLibraries
     *      jasmine setup
     *  2 bower
     *  3 js
     *  4 spec-helpers
     *  5 specs
     *  6 templates
     */
    testLibraries: [
      'node_modules/jasmine-core/lib/jasmine-core/jasmine.js',
      'node_modules/jasmine-core/lib/jasmine-core/jasmine-html.js',
      'node_modules/jasmine-core/lib/jasmine-core/boot.js'
    ],
    specHelpers: [src + 'test-helpers/*.js'],
    specs: [app + '**/*.spec.js'],

    /**
     * Mock Locations
     */
    mocks: {
      ngMock: './bower_components/angular-mocks/angular-mocks.js',
      mockdata: app + '**/*.mockdata.js',
      mockhttp: app + '**/*.mockhttp.js',
      mockHelpers: [
        src + 'helpers/*.js',                 // generic helpers
        src + 'helpers/mock-helpers/**/*.js'  // helpers specific for mocks
      ]
    },

    /**
     * plato
     */
    plato: {js: app + '**/*.js'},

    /**
     * Browser Sync
     */
    browserReloadDelay: 1000,

    /**
     * Node settings
     */
    defaultPort: '4000',

    /**
     * Rsync
     */

    rsyncConf: {
      root: 'build',
      progress: true,
      incremental: true,
      relative: true,
      emptyDirectories: true,
      recursive: true,
      clean: true,
    }
  };

  /**
   * wiredep and bower settings
   */
  config.getWiredepOptions = function() {
    var options = {
      bowerJson: config.bower.json,
      directory: config.bower.directory,
      ignorePath: config.bower.ignorePath,
      exclude: config.bower.exclude
    };
    return options;
  };

  /**
   * Karma Settings
   */
  config.karma = getKarmaOptions();

  return config;

  /////////////////////////////////////

  function getKarmaOptions() {
    var options = {
      files: [].concat(
        bowerFiles,
        config.specHelpers,
        app + '**/*.module.js',
        app + '**/!(*.mockhttp).js',    // exclude http mocks, as it may collide with the specs
        config.appConf.jsFile,
        temp + config.templateCache.file
      ),
      exclude: [],
      coverage: {
        dir: report + 'coverage',
        reporters: [
            // reporters not supporting the `file` property
            {type: 'html', subdir: 'report-html'},
            {type: 'lcov', subdir: 'report-lcov'},
            {type: 'text-summary'} //, subdir: '.', file: 'text-summary.txt'}
        ]
      },
      preprocessors: {}
    };

    options.preprocessors[app + '**/!(*.spec|*.mockdata|*.mockhttp)+(.js)'] = ['coverage'];

    return options;
  }
};
