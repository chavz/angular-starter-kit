(function() {
  'use strict';

  angular
    .module('app.core', [
      // Angular modules
      'ngSanitize',

      // App Shared Modules
      'blocks.logger',
      'blocks.exception',
      'blocks.router',
      'blocks.wrapper',

      // 3rd Party Modules
      'ui.bootstrap',
      'ui.router',

      // App Configuration
      'configurations'
    ]);

})();
