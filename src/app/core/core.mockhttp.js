(function() {
  'use strict';

  angular
    .module('app.core')
    .run(coreMock);

  /* @ngInject */
  function coreMock ($httpBackend) {
    // during end-to-end testing or backendless development,
    // issue real http request when requesting for html pages
    $httpBackend.whenGET(/\.html/).passThrough();
  }
})();
