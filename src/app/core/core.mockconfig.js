(function () {
  'use strict';

  var core = angular.module('app.core');

  core.config($httpConfig);

  ///////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////// $http Configurations ///////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  $httpConfig.$inject = ['$httpProvider'];
  /* @ngInject */
  function $httpConfig ($httpProvider) {
    // register an interceptor which logs http requests w/ request & response data automatically
    $httpProvider.interceptors.push(/*@ngInject*/ function ($injector) {
      return {
        'response': function (response) {
          // Let views through immediately
          if (typeof response.data === 'string' || response.data instanceof String) {
            return response;
          }
          // injected manually to get around circular dependency.
          var logger = $injector.get('logger');
          // proceed on logging the response with the request and response data
          logger.info(
            [response.config.method, '->', response.config.url,
              response.status, response.statusText].join(' '),
            {request: response.config, response: response}
          );
          // return response
          return response;
        }
      };
    });
  }

})();
