(function () {
  'use strict';

  var core = angular.module('app.core');

  var config = {
    appErrorPrefix: '[app Error] '
  };
  core.value('config', config);

  // load the different configurations into the application
  core.config(coreConfig);
  core.config(e2eConfig);

  ///////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////// Exception Handler Configurations////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  /* @ngInject */
  function coreConfig($logProvider, exceptionHandlerProvider) {
    // set error prefix for the exceptionHandler
    exceptionHandlerProvider.configure(config.appErrorPrefix);
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////// Backendless Mode Configurations ////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////

  /* @ngInject */
  function e2eConfig ($provide, APPCONFIG) {
    // if app is in end-to-end testing or backendless development mode
    if (APPCONFIG.E2E === true) {
      // overwrite the default $httpBackend service with the one provided by angular mocks
      $provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator);
    }
  }


})();
