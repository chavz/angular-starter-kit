'use strict';

/* jshint -W117, -W030 */
describe('Core', function () {
  describe('state', function() {
    // dependencies that we'll be needing
    var $state;
    var $location;
    var $rootScope;
    var $templateCache;

    var views = {
      four0four: 'app/core/404.html'
    };

    // load the module
    beforeEach(module('app.core'));

    // inject our dependencies and assign them properly
    beforeEach(inject(function (_$state_, _$location_, _$rootScope_, _$templateCache_) {
      $state = _$state_;
      $location = _$location_;
      $rootScope = _$rootScope_;
      $templateCache = _$templateCache_;
    }));

    beforeEach(function() {
      $templateCache.put(views.core, '');
    });

    it('should map /404 route to 404 View template', function() {
      expect($state.get('404').templateUrl).toEqual(views.four0four);
    });

    it('should work with $state.go', function() {
      $state.go('404');
      $rootScope.$apply();
      expect($state.is('404')).toBeTruthy();
    });

    it('should route /invalid to the otherwise (404) route', function() {
      $location.path('/invalid');
      $rootScope.$apply();
      expect($state.current.templateUrl).toEqual(views.four0four);
    });

  });
});
