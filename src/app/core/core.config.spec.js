'use strict';

/* jshint -W117, -W030 */
describe('core.config', function() {
  describe('with E2E mode turned on', function() {
    // inject and mock the configurations module
    beforeEach(module('configurations', function ($provide) {
      var appconfig = {E2E: true};
      $provide.constant('APPCONFIG', appconfig);
    }));

    // inject the module
    beforeEach(module('app.core'));

    it('should overwrite $httpBackend', inject(function($httpBackend) {
      expect($httpBackend.when().passThrough).toBeDefined();
    }));
  });

  describe('with E2E mode turned off', function() {
    // inject and mock the configurations module
    beforeEach(module('configurations', function ($provide) {
      var appconfig = {E2E: false};
      $provide.constant('APPCONFIG', appconfig);
    }));

    // inject the module
    beforeEach(module('app.core'));

    it('should not overwrite $httpBackend', inject(function($httpBackend) {
      expect($httpBackend.when().passThrough).toBeUndefined();
    }));
  });

});
