'use strict';

/* jshint -W117, -W030 */
describe('Home', function() {
  describe('state', function() {
    // dependencies that we'll be needing
    var $state;
    var $rootScope;
    var $templateCache;

    var view = 'app/home/home.html';

    // load the module
    beforeEach(module('app.home'));

    // inject our dependencies and assign them properly
    beforeEach(inject(function (_$state_, _$rootScope_, _$templateCache_) {
      $state = _$state_;
      $rootScope = _$rootScope_;
      $templateCache = _$templateCache_;
    }));

    beforeEach(function() {
      $templateCache.put(view, '');
    });

    it('should map state home to url / ', function() {
      expect($state.href('home', {})).toEqual('/');
    });

    it('should map /home route to home View template', function() {
      expect($state.get('home').templateUrl).toEqual(view);
    });

    it('of home should work with $state.go', function () {
      $state.go('home');
      $rootScope.$apply();
      expect($state.is('home')).toBeTruthy();
    });
  });
});
