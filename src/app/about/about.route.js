(function() {
  'use strict';

  angular
    .module('app.about')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [{
      state: 'about',
      config: {
        url: '/about',
        templateUrl: 'app/about/about.html',
        title: 'about',
        navSettings: {
          position: 2,
          content: 'About'
        }
      }
    }];
  }

})();
