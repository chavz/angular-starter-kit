'use strict';

/* jshint -W117, -W030 */
describe('About', function() {
  describe('state', function() {
    // dependencies that we'll be needing
    var $state;
    var $rootScope;
    var $templateCache;

    var view = 'app/about/about.html';

    // load the module
    beforeEach(module('app.about'));

    // inject our dependencies and assign them properly
    beforeEach(inject(function (_$state_, _$rootScope_, _$templateCache_) {
      $state = _$state_;
      $rootScope = _$rootScope_;
      $templateCache = _$templateCache_;
    }));

    beforeEach(function() {
      $templateCache.put(view, '');
    });

    it('should map state about to url /about ', function() {
      expect($state.href('about', {})).toEqual('/about');
    });

    it('should map /about route to about View template', function() {
      expect($state.get('about').templateUrl).toEqual(view);
    });

    it('of about should work with $state.go', function () {
      $state.go('about');
      $rootScope.$apply();
      expect($state.is('about')).toBeTruthy();
    });
  });
});
