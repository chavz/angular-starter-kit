(function() {

  'use strict';

  angular.module('app', [
    // Shared Modules
    'app.core',

    // Feature Modules
    'app.home',
    'app.about',
    'app.layout'
  ]);

})();
