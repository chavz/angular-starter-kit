'use strict';

/* jshint -W117, -W030 */
describe('blocks.wrapper', function() {
  var ngWrapProvider;
  var ngWrap;
  var $window;
  var mocks = {
    windowProperty: {
      testProperty: 'testValue'
    }
  };

  beforeEach(module('blocks.wrapper', function (_ngWrapProvider_) {
    ngWrapProvider = _ngWrapProvider_;
  }));

  beforeEach(inject(function (_ngWrap_, _$window_) {
    ngWrap = _ngWrap_;
    $window = _$window_;
  }));

  describe('ngWrapProvider', function() {
    it('should successfuly be defined', inject(function() {
      expect(ngWrapProvider).toBeDefined();
    }));

    it('should have a wrapper method', function() {
      expect(ngWrapProvider.wrapper).toBeDefined();
    });
  });

  describe('ngWrap', function() {
    it('should successfuly be defined', function() {
      expect(ngWrap).toBeDefined();
    });

    it('should have a wrapper method', function() {
      expect(ngWrap.wrapper).toBeDefined();
    });
  });

  it('should have ngWrapProvider.wrapper & ngWrap.wrapper point to the same function', function() {
    expect(ngWrapProvider.wrapper).toEqual(ngWrap.wrapper);
  });

  describe('wrapper method with .wrapper(window.property")', function() {
    it('should throw an error when window.property is undefined', function() {
      var windowPropertyName = 'thing';
      expect(function() { ngWrapProvider.wrapper(windowPropertyName); })
        .toThrow(new Error('Cannot find window.' + windowPropertyName));
      expect(function() { ngWrap.wrapper(windowPropertyName); })
        .toThrow(new Error('Cannot find window.' + windowPropertyName));
    });

    describe('with window.property', function() {
      beforeEach(inject(function($injector) {
        $window.thing = mocks.windowProperty;
        $injector.invoke(function () {
          ngWrapProvider.wrapper('thing');
        });
      }));

      it('should expose window.property as an angular service', inject(function(thing) {
        expect(thing).toBeDefined();
        expect(thing).toEqual(mocks.windowProperty);
      }));

      it('should delete window.property by default', function() {
        expect($window.thing).toBeUndefined();
      });
    });

    describe('with leaveGlobal', function() {
      beforeEach(inject(function($injector) {
        $window.thing = mocks.windowProperty;
        $injector.invoke(function () {
          ngWrapProvider.wrapper('thing', true);
        });
      }));

      it('should not delete window.property', function() {
        expect($window.thing).toBeDefined();
      });
    });
  });
});
