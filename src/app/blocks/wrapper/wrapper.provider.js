(function() {
  'use strict';

  angular
    .module('blocks.wrapper')
    .provider('ngWrap', ngWrapProvider);

  /* @ngInject */
  function ngWrapProvider($provide) {
    /* jshint validthis:true */
    this.wrapper = wrapper;

    this.$get = function() {
      return {'wrapper': wrapper};
    };

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////// IMPLEMENTATION ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    function wrapper(name, leaveGlobal) {
      $provide.provider(name, function () {
        if (typeof window[name] === 'undefined') {
          throw new Error('Cannot find window.' + name);
        }

        var thing = window[name];
        if (!leaveGlobal) {
          delete window[name];
        }

        this.$get = function() {
          return thing;
        };
      });
    }
  }
})();
