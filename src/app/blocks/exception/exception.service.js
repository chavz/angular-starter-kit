// Copyright (C) 2014 John Papa
//
// This file contains substantial parts from `johnpapa/pluralsight-gulp`,
// released under the MIT License (MIT).
//
// `chavz/angular-starter-kit` is distributed under
// the MIT License (MIT), for more info, see LICENSE.

/**
 * Exception Factory
 *
 * @namespace Factories
 */
(function() {
  'use strict';

  angular
    .module('blocks.exception')
    .factory('exception', exception);

  /**
   * @name   exception
   * @desc   Application wide exception handler and catcher
   *
   * @namespace Exception
   * @memberOf  Factories
   *
   * @ngInject
   */
  function exception(logger) {
    var service = {
      catcher: catcher
    };

    return service;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////// IMPLEMENTATION ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @name   catcher
     * @desc   Catches exception and logs them onto the console
     *
     * @param  {String} message The message to log
     *
     * @memberOf Factories.Exception
     */
    function catcher(message) {
      return function(reason) {
        logger.error(message, reason);
      };
    }
  }
})();
