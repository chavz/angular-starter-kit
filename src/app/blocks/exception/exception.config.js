// Copyright (C) 2014 John Papa
//
// This file contains substantial parts from `johnpapa/pluralsight-gulp`,
// released under the MIT License (MIT).
//
// `chavz/angular-starter-kit` is distributed under
// the MIT License (MIT), for more info, see LICENSE.

(function() {
  'use strict';

  angular
    .module('blocks.exception')
    .config(exceptionConfig);

  exceptionConfig.$inject = ['$provide'];

  /**
   * Configure by setting an optional string value for appErrorPrefix.
   * Accessible via config.appErrorPrefix (via config value).
   * @param  {Object} $provide
   * @return {Object}
   * @ngInject
   */
  function exceptionConfig($provide) {
    $provide.decorator('$exceptionHandler', extendExceptionHandler);
  }

  extendExceptionHandler.$inject = ['$delegate', 'exceptionHandler', 'logger'];

  /**
   * Extend the $exceptionHandler service to also display in our logger.
   *
   * @param  {Object} $delegate
   * @param  {Object} exceptionHandler
   * @param  {Object} logger
   *
   * @return {Function} the decorated $exceptionHandler service
   *
   * @ngInject
   */
  function extendExceptionHandler($delegate, exceptionHandler, logger) {
    return function(exception, cause) {
      var appErrorPrefix = exceptionHandler.config.appErrorPrefix || '';
      var errorData = {
        exception: exception,
        cause: cause
      };
      exception.message = appErrorPrefix + exception.message;

      /**
       * Could add the error to a service's collection,
       * add errors to $rootScope, log errors to remote web server,
       * or log locally. Or throw hard. It is entirely up to you.
       * throw exception;
       *
       * @example
       *     throw { message: 'error message we added' };
       */
      logger.error(exception.message, errorData);
      $delegate(exception, cause);
    };
  }
})();
