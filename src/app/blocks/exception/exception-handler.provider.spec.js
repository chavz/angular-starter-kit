'use strict';

/* jshint -W117, -W030 */
describe('blocks.exception', function() {
  var exceptionHandlerProvider;
  var mocks = {
    prefix: '[TEST]'
  };

  beforeEach(module('blocks.exception', function (_exceptionHandlerProvider_) {
    exceptionHandlerProvider = _exceptionHandlerProvider_;
  }));

  describe('exceptionHandlerProvider', function() {

    it('should be defined', inject(function() {
      expect(exceptionHandlerProvider).toBeDefined();
    }));

    it('should have a configuration', function() {
      expect(exceptionHandlerProvider.config).toBeDefined();
    });

    describe('with appErrorPrefix', function() {
      beforeEach(function() {
        exceptionHandlerProvider.configure(mocks.prefix);
      });

      it('it should have an appErrorPrefix defined', function() {
        expect(exceptionHandlerProvider.$get().config.appErrorPrefix).toBeDefined();
      });

      it('should have appErrorPrefix set properly', function() {
        expect(exceptionHandlerProvider.$get().config.appErrorPrefix).toEqual(mocks.prefix);
      });
    });
  });

});
