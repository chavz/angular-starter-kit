'use strict';

/* jshint -W117, -W030 */
describe('blocks.exception', function() {
  var exception;
  var logger;

  var mocks = {
    message: 'Test',
    reason: 'test'
  };

  beforeEach(module('blocks.exception'));

  beforeEach(inject(function (_exception_, _logger_) {
    exception = _exception_;
    logger = _logger_;

    spyOn(logger, 'error');
  }));

  describe('exception', function() {
    it('should be successfully defined', function() {
      expect(exception).toBeDefined();
    });

    it('should expose an exception.catcher', function() {
      expect(exception.catcher).toBeDefined();
    });

    describe('exception.catcher', function() {
      it('should log an error containing the exception', function() {
        exception.catcher(mocks.message)(mocks.reason);
        expect(logger.error).toHaveBeenCalledWith(mocks.message, mocks.reason);
      });
    });
  });
});
