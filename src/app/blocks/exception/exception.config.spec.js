'use strict';

/* jshint -W117, -W030 */
describe('blocks.exception', function() {
  var exceptionHandlerProvider;
  var $rootScope;
  var logger;

  var mocks = {
    prefix: '[TEST]',
    errorMessage: 'fake error',
  };

  beforeEach(module('blocks.exception'));

  beforeEach(inject(function (_$rootScope_, _logger_) {
    $rootScope = _$rootScope_;
    logger = _logger_;

    spyOn(logger, 'error');
  }));

  describe('$exceptionHandler', function() {
    it('should throw an error when forced', inject(function() {
      expect(functionThatWillThrow).toThrow();
    }));

    it('should use a decorator to handle manual error', function() {
      try {
        $rootScope.$apply(functionThatWillThrow);
      }
      catch (ex) {
        expect(ex.message).toEqual(mocks.errorMessage);
      }
    });

    it('should log an error when an exception is thrown', function() {
      var exception;
      try {
        $rootScope.$apply(functionThatWillThrow);
      } catch (ex) {
        exception = ex;
      } finally {
        var errorData = {
          exception: exception,
          cause: undefined
        };
        expect(logger.error).toHaveBeenCalledWith(mocks.errorMessage, errorData);
      }
    });
  });

  function functionThatWillThrow() {
    throw new Error(mocks.errorMessage);
  }
});
