'use strict';

/* jshint -W117, -W030 */
describe('factory: Logger', function() {
  var logger;
  var $log;

  beforeEach(module('blocks.logger'));

  beforeEach(inject(function (_logger_, _$log_) {
    logger = _logger_;
    $log = _$log_;

    spyOn($log, 'error');
    spyOn($log, 'info');
    spyOn($log, 'warn');
  }));

  it('should log an error message with undefined object', function() {
    var msg = 'Hello';
    logger.error(msg);
    expect($log.error).toHaveBeenCalledWith('Error: ' + msg, undefined);
  });

  it('should log an error message accompanied with an empty object', function() {
    var msg = 'Hello';
    logger.error(msg, {});
    expect($log.error).toHaveBeenCalledWith('Error: ' + msg, {});
  });

  it('should log an info message with undefined object', function() {
    var msg = 'Hello';
    logger.info(msg);
    expect($log.info).toHaveBeenCalledWith('Info: ' + msg, undefined);
  });

  it('should log an info message accompanied with an empty object', function() {
    var msg = 'Hello';
    logger.info(msg, {});
    expect($log.info).toHaveBeenCalledWith('Info: ' + msg, {});
  });

  it('should log an success message with undefined object', function() {
    var msg = 'Hello';
    logger.success(msg);
    expect($log.info).toHaveBeenCalledWith('Success: ' + msg, undefined);
  });

  it('should log an success message accompanied with an empty object', function() {
    var msg = 'Hello';
    logger.success(msg, {});
    expect($log.info).toHaveBeenCalledWith('Success: ' + msg, {});
  });

  it('should log an warning message with undefined object', function() {
    var msg = 'Hello';
    logger.warning(msg);
    expect($log.warn).toHaveBeenCalledWith('Warning: ' + msg, undefined);
  });

  it('should log an warning message accompanied with an empty object', function() {
    var msg = 'Hello';
    logger.warning(msg, {});
    expect($log.warn).toHaveBeenCalledWith('Warning: ' + msg, {});
  });
});
