// Copyright (C) 2014 John Papa
//
// This file contains substantial parts from `johnpapa/pluralsight-gulp`,
// released under the MIT License (MIT).
//
// `chavz/angular-starter-kit` is distributed under
// the MIT License (MIT), for more info, see LICENSE.

/**
 * Logger Factory
 *
 * @namespace Factories
 */
(function() {
  'use strict';

  angular
    .module('blocks.logger')
    .factory('logger', logger);

  /**
   * @name   logger
   * @desc   Application wide logger
   *
   * @namespace Logger
   * @memberOf  Factories
   *
   * @ngInject
   */
  function logger($log) {
    var service = {

      error   : error,
      info    : info,
      success : success,
      warning : warning,

      // straight to console;
      log     : $log.log
    };

    return service;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////// IMPLEMENTATION ///////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @name   error
     * @desc   Logs errors onto the console
     *
     * @param  {String}   message   The message to log
     * @param  {Object}   data      The data to log
     *
     * @memberOf Factories.Logger
     */
    function error(message, data) {
      $log.error('Error: ' + message, data);
    }

    /**
     * @name   info
     * @desc   Logs info onto the console
     *
     * @param  {String} message The message to log
     * @param  {Object} data    The data to log
     *
     * @memberOf Factories.Logger
     */
    function info(message, data) {
      $log.info('Info: ' + message, data);
    }

    /**
     * @name   success
     * @desc   Logs success info's onto the console
     *
     * @param  {String} message The message to log
     * @param  {Object} data    The data to log
     *
     * @memberOf Factories.Logger
     */
    function success(message, data) {
      $log.info('Success: ' + message, data);
    }

    /**
     * @name   warning
     * @desc   Logs warnings onto the console
     *
     * @param  {String} message The message to log
     * @param  {Object} data    The data to log
     *
     * @memberOf Factories.Logger
     */
    function warning(message, data) {
      $log.warn('Warning: ' + message, data);
    }
  }
})();
