'use strict';

/* jshint -W117, -W030 */
describe('blocks.router', function() {
  var $state;
  var $rootScope;
  var routerHelperProvider;

  beforeEach(module('blocks.router', function (_routerHelperProvider_) {
    routerHelperProvider = _routerHelperProvider_;
  }));

  // inject our dependencies and assign them properly
  beforeEach(inject(function (_$state_, _$rootScope_) {
    $state = _$state_;
    $rootScope = _$rootScope_;
  }));

  describe('routerHelperProvider', function() {
    it('should successfuly be defined', inject(function() {
      expect(routerHelperProvider).toBeDefined();
    }));

    describe('with resolveAlways: {testResolve: "This will always be resolved"}', function() {
      beforeEach(function() {
        var config = {resolveAlways: {testResolve: 'This will always be resolved'}};
        routerHelperProvider.configure(config);
      });
      it('should have all states to resolve testResolve', inject(function (routerHelper) {
        routerHelper.configureStates(mockData.getMockStates(), '/');
        var states = routerHelper.getStates();
        states.forEach(function (state) {
          if (state.url === '^') { return; }
          expect(state.resolve.hasOwnProperty('testResolve')).toBeTruthy();
          expect(state.resolve.testResolve).toEqual('This will always be resolved');
        });
      }));
    });
  });

  describe('routerHelper', function() {
    var routerHelper;
    beforeEach(inject(function (_routerHelper_) {
      routerHelper = _routerHelper_;
    }));

    it('should successfuly be defined', function() {
      expect(routerHelper).toBeDefined();
    });

    it('should have getStates() to retrieve all ui-router states', function() {
      expect(routerHelper.getStates()).toEqual($state.get());
    });
  });

  describe('with $stateChangeError', function() {
    var logger;
    var routerHelper;
    var $templateCache;

    beforeEach(inject(function (_routerHelper_, _logger_, _$templateCache_) {
      logger = _logger_;
      routerHelper = _routerHelper_;
      $templateCache = _$templateCache_;
    }));

    beforeEach(function() {
      routerHelper.configureStates(mockData.getMockStates(), '/');
      routerHelper.getStates().forEach(function (state) {
        $templateCache.put(state.templateUrl, '');
      });
      $rootScope.$apply();
    });

    beforeEach(function() {
      spyOn(logger, 'warning');
      $rootScope.$emit('$stateChangeError', {}, {}, {}, {}, {});
    });

    it('should call logger.warning', function() {
      expect(logger.warning).toHaveBeenCalled();
    });

    it('should map to /', function() {
      expect($state.current.url).toEqual('/');
    });

    it('should not entertain another $stateChangeError ' +
      'while still handling the previous one.', function() {
        $rootScope.$emit('$stateChangeError', {}, {}, {}, {}, {data: 'second call'});
        expect(logger.warning)
          .not.toHaveBeenCalledWith('Error routing to unknown target. second call: ', [{}]);
      });
  });
});
