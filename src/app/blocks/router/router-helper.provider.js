// Copyright (C) 2014 John Papa
//
// This file contains substantial parts from `johnpapa/pluralsight-gulp`,
// released under the MIT License (MIT).
//
// `chavz/angular-starter-kit` is distributed under
// the MIT License (MIT), for more info, see LICENSE.

(function() {
  'use strict';

  angular
    .module('blocks.router')
    .provider('routerHelper', routerHelperProvider);

  /* @ngInject */
  function routerHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
    /* jshint validthis:true */

    // App wide router configurations
    var config = {
      resolveAlways: {}
    };

    // Let this app wide configuration be extendible
    this.configure = function(cfg) {
      angular.extend(config, cfg);
    };

    // use the HTML5 History API
    // this removes the hashbang in the url
    // for browsers that support `pushState`.
    $locationProvider.html5Mode(true);

    this.$get = RouterHelper;

    /* @ngInject */
    function RouterHelper($location, $rootScope, $state, logger) {
      var handlingStateChangeError = false;
      var hasOtherwise = false;

      var service = {
        configureStates: configureStates,
        getStates: getStates
      };

      init();

      return service;

      //////////////////////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////// IMPLEMENTATION ///////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////////////////////////

      function configureStates(states, otherwisePath) {
        states.forEach(function(state) {
          state.config.resolve = angular.extend(state.config.resolve || {}, config.resolveAlways);
          $stateProvider.state(state.state, state.config);
        });

        if (otherwisePath && !hasOtherwise) {
          hasOtherwise = true;
          $urlRouterProvider.otherwise(otherwisePath);
        }
      }

      function handleRoutingErrors() {
        // Route cancellation:
        // On routing error, go to the root url.
        // Provide an exit clause if it tries to do it twice.
        $rootScope.$on('$stateChangeError',
          function(event, toState, toParams, fromState, fromParams, error) {
            if (handlingStateChangeError) {
              return;
            }
            event.preventDefault();
            handlingStateChangeError = true;

            var destination = (toState &&
              (toState.title || toState.name || toState.loadedTemplateUrl)) ||
              'unknown target';

            var msg = 'Error routing to ' + destination + '. ' +
              (error.data || '') + (error.statusText || '') +
              ': ' + (error.status || '');

            logger.warning(msg, [toState]);
            $location.path('/');
          }
        );

        $rootScope.$on('$stateChangeSuccess',
          function(event, toState, toParams, fromState, fromParams) {
            handlingStateChangeError = false;
          }
        );
      }

      function getStates() {
        return $state.get();
      }

      function init() {
        handleRoutingErrors();
      }
    }

  }

})();
