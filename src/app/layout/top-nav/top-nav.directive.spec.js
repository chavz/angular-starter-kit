'use strict';

/* jshint -W117, -W030 */
describe('topNav', function() {
  describe('topNav Directive', function() {
    var $compile;
    var $rootScope;
    var $controller;
    var routerHelper;

    var el;
    var controller;

    // Load the app.layout module, which contains the directive
    beforeEach(module('app.layout'));

    // Store references to $rootScope and $compile
    // so they are available to all tests in this describe block
    beforeEach(inject(function (_$compile_, _$rootScope_, _routerHelper_, _$controller_) {
      // The injector unwraps the underscores (_) from around the parameter names when matching
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      $controller = _$controller_;
      routerHelper = _routerHelper_;
    }));

    beforeEach(function () {
      routerHelper.configureStates(mockData.getMockStates(), '/');
      controller = $controller('TopNavController');
    });

    beforeEach(function () {
      // The minimum necessary template HTML for this spec.
      el = angular.element('<top-nav></top-nav>');

      // ng's $compile service resolves nested directives (there are none in this example)
      // and binds the element to the scope (which must be a real ng scope)
      $compile(el)($rootScope);

      // tell angular to look at the scope values right now
      $rootScope.$digest();
    });

    it('should list all routes with navSettings config', function() {
      expect(el.find('li').length).toEqual(controller.navRoutes.length);
    });
  });
});
