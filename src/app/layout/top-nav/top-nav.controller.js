(function() {
  'use strict';

  angular
    .module('app.layout')
    .controller('TopNavController', TopNavController);

  TopNavController.$inject = ['$state', 'routerHelper'];

  /* @ngInject */
  function TopNavController($state, routerHelper) {
    var vm = this;
    var states = routerHelper.getStates();

    getNavRoutes();
    /////////////////////////////////

    function getNavRoutes() {
      vm.navRoutes = states.filter(function(r) {
        return r.navSettings && r.navSettings.position;
      }).sort(function(r1, r2) {
        return r1.navSettings.position - r2.navSettings.position;
      });
    }
  }

})();
