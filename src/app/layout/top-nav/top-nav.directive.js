(function() {
  'use strict';

  angular
    .module('app.layout')
    .directive('topNav', topNav);

  /* @ngInject */
  function topNav () {
    var directive = {
      // bindToController: true,
      controller: 'TopNavController',
      controllerAs: 'vm',
      restrict: 'EA',
      templateUrl: 'app/layout/top-nav/top-nav.html'
    };

    return directive;
  }

})();
