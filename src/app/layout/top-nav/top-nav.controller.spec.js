'use strict';

/* jshint -W117, -W030 */
describe('topNav', function() {
  describe('topNav Controller', function() {
    var $controller;
    var $state;
    var routerHelper;

    var controller;

    // load the controller's module
    beforeEach(module('app.layout'));

    beforeEach(inject(function (_$controller_, _$state_, _routerHelper_) {
      $controller = _$controller_;
      $state = _$state_;
      routerHelper = _routerHelper_;
    }));

    beforeEach(function () {
      routerHelper.configureStates(mockData.getMockStates(), '/');
      controller = $controller('TopNavController');
    });

    it('should be created successfully', function () {
      expect(controller).toBeDefined();
    });

    it('should have navRoutes to contain only all ui-router states ' +
       'w/ navSettings && navSettings.position config', function() {
      var states = controller.navRoutes.filter(function (r) {
        return r.navSettings && r.navSettings.position;
      });
      expect(controller.navRoutes.length).toEqual(states.length);
    });

    it('should have navRoutes be sorted according to navSettings.position in ASC', function() {
      var currentPosition;
      var succeddingPosition;
      for (var n = 0; n < controller.navRoutes.length - 1; n++) {
        currentPosition = controller.navRoutes[n].navSettings.position;
        succeddingPosition = controller.navRoutes[n + 1].navSettings.position;
        expect(currentPosition).toBeLessThan(succeddingPosition);
      }
    });
  });
});
