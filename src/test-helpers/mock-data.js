/* jshint -W079 */
var mockData = (function() {
  return {
    getMockStates: getMockStates
  };

  function getMockStates() {
    return [
      {
        state: 'home',
        config: {
          url: '/',
          templateUrl: 'app/home/home.html',
          title: 'home',
          navSettings: {
            position: 1,
            content: 'Home'
          }
        }
      },
      {
        state: 'about',
        config: {
          url: '/about',
          templateUrl: 'app/about/about.html',
          title: 'about',
          navSettings: {
            position: 2,
            content: 'About'
          }
        }
      },
      {
        state: 'stateWithoutNavSettings',
        config: {
          url: '/without-nav-settings',
          title: 'Without Nav Settings'
        }
      },
      {
        state: 'stateWithoutNavSettingsPosition',
        config: {
          url: '/without-nav-settings-position',
          title: 'Without Nav Settings Position',
          navSettings: {
            content: 'Without Nav Settings Position'
          }
        }
      }
    ];
  }

})();
