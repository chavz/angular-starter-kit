## 0.1.0
* Initial Commit

## 0.2.0
* add gulp task for automated deployment
* add fontawesome into the dependecy
* improve `.gitignore` in handling different IDE's

## 0.3.0
* upgrades package.json
* upgrades to angular 1.5
