// Copyright (C) 2014 John Papa
//
// This file contains substantial parts from `johnpapa/pluralsight-gulp`,
// released under the MIT License (MIT).
//
// `chavz/angular-starter-kit` is distributed under
// the MIT License (MIT), for more info, see LICENSE.

var fs = require('fs');
var config = require('./gulp.config')();
var conf = (fs.existsSync('./appconf.json')) ? require('./appconf.json') : undefined;
var gulp = require('gulp');
var del = require('del');
var args = require('yargs').argv;
var browserSync = require('browser-sync');
var $ = require('gulp-load-plugins')({lazy: true});

/**
 * yargs variables can be passed in to alter the behavior, when present.
 * Example: gulp analyze
 *
 * --verbose  : Various tasks will produce more output to the console.
 * --env      : Execute tasks using environment specific configurations.
 *              Possible values are `development` and `production`.
 *              Defaults to `development`.
 */

/**
 * List the available gulp tasks
 */
gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

/**
 * analyze the code and create coverage report
 * @return {Stream}
 */
gulp.task('analyze', function() {
  log('Analyzing source with JSHint and JSCS');

  return gulp
    .src(config.allScripts)
    .pipe($.if(args.verbose, $.print()))
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish', {verbose: true}))
    .pipe($.jshint.reporter('fail'))
    .pipe($.jscs());
});

/**
 * Create a visualizer report
 */
gulp.task('plato', function(done) {
  log('Analyzing source with Plato');
  log('Browse to /report/plato/index.html to see Plato results');

  startPlatoVisualizer(done);
});

/**
 * AutoPrefix CSS
 * @return {Stream}
 */
gulp.task('styles', ['clean-styles'], function() {

  return gulp
    .src(config.appStyles)
    .pipe($.plumber()) // exit gracefully if something fails after this
    .pipe($.autoprefixer({browsers: ['last 2 version', '> 5%']}))
    .pipe(gulp.dest(config.temp));
});

/**
 * Copy fonts
 * @return {Stream}
 */
gulp.task('fonts', ['clean-fonts'], function() {
  log('Copying fonts');

  return gulp
    .src(config.appFonts)
    .pipe(gulp.dest(config.build + 'fonts'));
});

/**
 * Compress images
 * @return {Stream}
 */
gulp.task('images', ['clean-images'], function() {
  log('Compressing and copying images');

  return gulp
    .src(config.appImages)
    .pipe($.imagemin({optimizationLevel: 4}))
    .pipe(gulp.dest(config.build + 'images'));
});

/**
 * Create $templateCache from the html templates
 * @return {Stream}
 */
gulp.task('templatecache', ['clean-code'], function() {
  log('Creating an AngularJS $templateCache');

  return gulp
    .src(config.htmltemplates)
    .pipe($.if(args.verbose, $.bytediff.start()))
    .pipe($.minifyHtml({empty: true}))
    .pipe($.if(args.verbose, $.bytediff.stop(bytediffFormatter)))
    .pipe($.angularTemplatecache(
      config.templateCache.file,
      config.templateCache.options
    ))
    .pipe(gulp.dest(config.temp));
});

gulp.task('e2e', function () {
  log('Checking if E2E mode is turned on ...');

  var stream = gulp.src(config.indexHtml);
  var e2eMode = (typeof conf === 'undefined') ? false : conf.APPCONFIG.E2E;

  if (e2eMode) {
    log('E2E mode is turned on, injecting mocks ...');

    var mockfiles = [].concat(
      config.mocks.mockdata,
      config.mocks.mockhttp
    );

    stream = stream
      .pipe(inject(config.mocks.ngMock, {name: 'inject:ngMock'}))
      .pipe(inject(config.mocks.mockHelpers, {name: 'inject:mockHelpers'}))
      .pipe(inject(mockfiles, {name: 'inject:mocks'}));
  } else {
    log('E2E mode is turned off, cleaning ...');

    var regex = /<!-- inject:(([\w]+)?(M|m)ock([\w]+)?):js -->([\S\s]*?)<!-- endinject -->/igm;
    var replacement = '<!-- inject:$1:js -->\n  <!-- endinject -->';

    stream = stream.pipe($.replace(regex, replacement));
  }
  return stream.pipe(gulp.dest(config.src));
});

/**
 * Wire-up the bower dependencies
 * @return {Stream}
 */
gulp.task('wiredep', ['e2e'], function() {
  log('Wiring the bower dependencies into the html');

  var options = config.getWiredepOptions();
  var wiredep = require('wiredep').stream;

  return gulp
    .src(config.indexHtml)
    .pipe(wiredep(options))
    .pipe(gulp.dest(config.src));
});

/**
 * Create angular constants from application config file
 * @return {Stream}
 */
gulp.task('appconf', ['clean-code'], function() {
  log('Creating file constants as app configurations');

  var environment;

  // determine which environment configuration should be loaded
  if (args.env && config.appConf.environments.indexOf(args.env) >= 0) {
    environment = args.env;
  } else {
    environment = config.appConf.defaultConfig.environment;
  }

  var sourceFiles = [].concat(
    // load the general default configurations first
    config.appConf.defaultConfig.file,
    // then the environment specific configurations
    config.appConf.defaultConfig.folder + environment + '.json',
    // and finally our custom configurations, if any.
    config.appConf.jsonFile
  );

  // wrap the module in an IIFE
  var wrapper = '(function () {\n' +
    '\'use strict\';\n\n' +
    '<%= module %>\n' +
    '})();\n';

  var options = {
    createModule: true,
    wrap: wrapper
  };

  return gulp
    .src(sourceFiles)
    .pipe($.extend(config.appConf.filename))
    .pipe($.ngConfig(config.appConf.module, options))
    .pipe(gulp.dest(config.temp));
});

/**
 * Inject all custom scripts and css into the html,
 * as well as the application config file
 * @return {Stream}
 */
gulp.task('inject', ['wiredep', 'styles', 'templatecache', 'analyze', 'appconf'], function() {
  log('Wire up app scripts and css into the html, after files are ready');

  return gulp
    .src(config.indexHtml)
    .pipe(inject(config.appConf.jsFile, {name: 'inject:appconfig'}))
    .pipe(inject(config.appScripts, {}, config.appScriptsOrder))
    .pipe(inject(config.processedStyles, {}))
    .pipe(gulp.dest(config.src));
});

/**
 * Run specs once and exit
 * @return {Stream}
 */
gulp.task('test', ['analyze', 'templatecache', 'appconf'], function(done) {
  startTests(true /*singleRun*/ , done);
});

/**
 * Run specs and wait.
 * Watch for file changes and re-run tests on each change
 */
gulp.task('autotest', function(done) {
  startTests(false /*singleRun*/ , done);
});

/**
 * Optimize all files, move to a build folder,
 * and inject them into the new index.html
 * @return {Stream}
 */
gulp.task('optimize', ['inject', 'test'], function() {
  log('Optimizing the js, css, and html');

  var templateCache = config.temp + config.templateCache.file;
  var assets = $.useref.assets({searchPath: './'});

  // Filters are named for the gulp-useref path
  var cssFilter = $.filter('**/*.css', {restore: true});
  var jsAppFilter = $.filter('**/' + config.optimized.app, {restore: true});
  var jslibFilter = $.filter('**/' + config.optimized.lib, {restore: true});

  return gulp
    .src(config.indexHtml)
    .pipe($.plumber()) // exit gracefully if something fails after this
    // Get the application config
    .pipe(inject(config.appConf.jsFile,
      {name: 'inject:appconfig'}))
    // Get the template cache
    .pipe(inject(templateCache,
      {name: 'inject:templates'}))
    .pipe(assets) // Gather all assets from the html with useref
    // Get the css
    .pipe(cssFilter)
    .pipe($.minifyCss()) // alternative is $.csso()
    .pipe(cssFilter.restore)
    // Get the app scripts
    .pipe(jsAppFilter)
    .pipe($.ngAnnotate({add: true, 'single_quotes': true}))
    .pipe($.uglify())
    .pipe(getHeader())
    .pipe(jsAppFilter.restore)
    // Get the vendor scripts
    .pipe(jslibFilter)
    .pipe($.uglify()) // another option is to override wiredep to use min files
    .pipe(jslibFilter.restore)
    // Take inventory of the file names for future rev numbers
    .pipe($.rev())
    // Apply the concat and file replacement with useref
    .pipe(assets.restore())
    .pipe($.useref())
    // Replace the file names in the html with rev numbers
    .pipe($.revReplace())
    .pipe(gulp.dest(config.build))
    // create a manifest to keep track of rev history
    .pipe($.rev.manifest())
    // Then send it all to the build folder
    .pipe(gulp.dest(config.build));
});

/**
 * Build everything
 * This is separate so we can run tests on
 * optimize before handling image or fonts
 */
gulp.task('build', ['optimize', 'images', 'fonts'], function() {
  log('Building everything');

  del(config.temp);

  log('Deployed to the build folder');
});

/**
 * Deploy build to a remote server
 * @return {Stream}
 */
gulp.task('deploy', ['build'], function () {
  // check for required parameters
  if (!!!args.destination) {
    throwError('deploy', 'Missing/invalid destination');
    return;
  }

  // load default configurations
  var conf = config.rsyncConf;
  // add the required parameters
  config.rsyncConf.destination = args.destination;
  // add optional parameters
  conf.hostname = !!args.hostname ? args.hostname : undefined;
  conf.username = !!args.username ? args.username : undefined;
  conf.port = !!args.port ? args.port : undefined;

  return gulp
    .src(config.build)
    .pipe($.rsync(conf));
});

/**
 * Inject all the spec files into the specs.html
 * @return {Stream}
 */
gulp.task('build-specs', ['templatecache', 'appconf'], function(done) {
  log('building the spec runner');

  var wiredep = require('wiredep').stream;
  var templateCache = config.temp + config.templateCache.file;
  var options = config.getWiredepOptions();
  var specs = config.specs;

  options.devDependencies = true;

  return gulp
    .src(config.specRunner)
    .pipe(wiredep(options))
    .pipe(inject(config.appScripts, {}, config.appScriptsOrder))
    .pipe(inject(config.appConf.jsFile,
      {name: 'inject:appconfig'}))
    .pipe(inject(config.testLibraries,
      {name: 'inject:testLibraries'}))
    .pipe(inject(config.mocks.mockdata,
      {name: 'inject:mockdata'}))
    .pipe(inject(config.specHelpers,
      {name: 'inject:specHelpers'}))
    .pipe(inject(config.specs,
      {name: 'inject:specs'}))
    .pipe(inject(templateCache,
      {name: 'inject:templates'}))
    .pipe(gulp.dest(config.src));

});

/**
 * Run the spec runner
 * @return {Stream}
 */
gulp.task('serve-specs', ['build-specs'], function(done) {
  log('run the spec runner');

  serveBrowserSync(true /* isDev */, true /* specRunner */);
  done();
});

/**
 * Serve the development environment
 */
gulp.task('serve-dev', ['inject'], function() {
  log('Serving development environment');

  serveBrowserSync(true /* isDev */, false /* specRunner */);
});

/**
 * Serve the build environment
 */
gulp.task('serve-build', ['build'], function() {
  log('Serving build environment');

  serveBrowserSync(false /* isDev */, false /* specRunner */);
});

/**
 * Bump the version
 * --type=pre will bump the prerelease version *.*.*-x
 * --type=patch or no flag will bump the patch version *.*.x
 * --type=minor will bump the minor version *.x.*
 * --type=major will bump the major version x.*.*
 * --version=1.2.3 will bump to a specific version and ignore other flags
 */
gulp.task('bump', function() {
  var msg = 'Bumping versions';
  var type = args.type;
  var version = args.version;
  var options = {};

  if (version) {
    options.version = version;
    msg += ' to ' + version;
  } else {
    options.type = type;
    msg += ' for a ' + type;
  }

  log(msg);

  return gulp
    .src(config.packages)
    .pipe($.print())
    .pipe($.bump(options))
    .pipe(gulp.dest(config.root));
});

/**
 * Create the documentation using JsDocs
 */
gulp.task('docs', function () {
  log('Creating JsDocs documentation.. to ./build/docs');

  var conf = 'jsdoc.conf.json';
  return gulp
    .src(config.jsDoc.conf.files)             // lets take all our config files
    .pipe($.order(config.jsDoc.conf.order))   // order them appropriately
    .pipe($.extend(conf))                     // then extend them to a single file
    .pipe(gulp.dest(config.temp))             // in the temp folder
    .pipe($.shell([
      config.jsDoc.exe +                  // jsdoc exe file
        ' -c ' + config.temp + conf +     // extended config file
        ' -t ' + config.jsDoc.template +  // template file
        ' -d ' + config.jsDoc.dest +      // output directory
        ' -r ' + config.app               // source code directory
    ]));
});

/**
 * Remove all files from the build, temp, and reports folders
 * @param  {Function} done - callback when complete
 */
gulp.task('clean', function(done) {
  var delconfig = [].concat(config.build, config.temp, config.report);
  log('Cleaning: ' + $.util.colors.blue(delconfig));
  del(delconfig, done);
});

/**
 * Remove all styles from the build and temp folders
 * @param  {Function} done - callback when complete
 */
gulp.task('clean-styles', function(done) {
  var files = [].concat(
    config.temp + '**/*.css',
    config.build + 'styles/**/*.css'
  );
  return clean(files, done);
});

/**
 * Remove all fonts from the build folder
 * @param  {Function} done - callback when complete
 */
gulp.task('clean-fonts', function(done) {
  return clean(config.build + 'fonts/**/*.*', done);
});

/**
 * Remove all images from the build folder
 * @param  {Function} done - callback when complete
 */
gulp.task('clean-images', function(done) {
  return clean(config.build + 'images/**/*.*', done);
});

/**
 * Remove all js and html from the build and temp folders
 * @param  {Function} done - callback when complete
 */
gulp.task('clean-code', function(done) {
  var files = [].concat(
    config.temp + '**/*.js',
    config.build + 'js/**/*.js',
    config.build + '**/*.html'
  );
  return clean(files, done);
});

///////////////////////////////////////////////////////////////////////////////

/**
 * Serve and Start BrowserSync
 */
function serveBrowserSync(isDev, specRunner) {
  log('Starting BrowserSync on port ' + config.defaultPort);

  var modRewrite = require('connect-modrewrite');

  var options = {
    server: {
      // if dev, serve under ./src else serve under ./build
      baseDir: isDev ? [config.src] : [config.build],
      routes: isDev ? { // serve these routes only on dev
        '/src': config.src,
        '/.tmp': config.temp,
        '/bower_components': config.bower.directory,
        '/node_modules': config.npm.directory
      } : {},
      middleware: [
        modRewrite([
          '^[^\\.]*$ /index.html [L]' //Matches everything that does not contain a '.' (period)
        ]),
      ]
    },
    port: 3000,
    open: false,
    notify: true,
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    // will present a certificate warning in the browser.
    // https: true,
    files: isDev ? [
      config.src + '**/*.*',    // watch for anything that moves
      '!' + config.appStyles,   // except for our raw styles
      config.temp + '**/*.css'  // instead watch our processed styles
    ] : [],
    ghostMode: { // these are the defaults t,f,t,t
      clicks: true,
      location: false,
      forms: true,
      scroll: true
    },
    injectChanges: true,
    logFileChanges: true,
    // logLevel: 'debug',
    logPrefix: 'app',
    reloadDelay: 1000 // 0
  };

  // If dev: watches css, runs it with autoprefixer, browser-sync handles reload
  // If build: watches the files, builds, and restarts browser-sync.
  if (isDev) {
    gulp.watch([config.appStyles], ['styles'])
      .on('change', changeEvent);
  } else {
    gulp.watch([
      config.appStyles,       // all our app css
      config.appScripts,      // all our app scripts
      config.allHtml,         // all our app html
      '!' + config.indexHtml  // except for the index.html, to avoid infinite loop.
    ], ['optimize', browserSync.reload]
    ).on('change', changeEvent);
  }

  if (specRunner) {
    options.startPath = config.specRunnerFile;
  }

  browserSync(options);
}

/**
 * Start the tests using karma.
 * @param  {boolean} singleRun - True means run once and end (CI), or keep running (dev)
 * @param  {Function} done - Callback to fire when karma is done
 * @return {undefined}
 */
function startTests(singleRun, done) {
  var KarmaServer = require('karma').Server;
  var excludeFiles = [];

  var karma = new KarmaServer({
    configFile: __dirname + '/karma.conf.js',
    exclude: excludeFiles,
    singleRun: !!singleRun
  }, karmaCompleted);

  karma.start();

  /////////////////////////////////////

  function karmaCompleted(karmaResult) {
    log('Karma completed');

    if (karmaResult === 1) {
      done('karma: tests failed with code ' + karmaResult);
    } else {
      done();
    }
  }
}

/**
 * Start Plato inspector and visualizer
 */
function startPlatoVisualizer(done) {
  log('Running Plato');

  var plato = require('plato');
  var glob = require('glob');

  var files = glob.sync(config.plato.js);
  var excludeFiles = /.*\.spec\.js/;

  var options = {
    title: 'Plato Inspections Report',
    exclude: excludeFiles
  };
  var outputDir = config.report + '/plato';

  plato.inspect(files, outputDir, options, platoCompleted);

  /////////////////////////////////////

  function platoCompleted(report) {
    var overview = plato.getOverviewReport(report);
    if (args.verbose) {
      log(overview.summary);
    }
    if (done) {
      done();
    }
  }
}

/**
 * Inject files in a sorted sequence
 * @param   {Array}   src       The glob pattern for source files
 * @param   {String}  options   The gulp-inject options
 * @param   {Array}   order     The glob pattern for sort order of the files
 * @returns {Stream}            The stream
 */
function inject(src, options, order) {
  return $.inject(orderSrc(src, order), options);
}

/**
 * Order a stream
 * @param   {Stream}  src   The gulp.src stream
 * @param   {Array}   order The glob array pattern
 * @returns {Stream}        The ordered stream
 */
function orderSrc (src, order) {
  return gulp
    .src(src)
    .pipe($.if(order, $.order(order)));
}

/**
 * Formatter for bytediff to display the size changes after processing
 * @param  {Object} data  byte data
 * @return {String}       Difference in bytes, formatted
 */
function bytediffFormatter(data) {
  var difference = (data.savings > 0) ? ' smaller.' : ' larger.';
  return data.fileName + ' went from ' +
    (data.startSize / 1000).toFixed(2) + ' kB to ' +
    (data.endSize / 1000).toFixed(2) + ' kB and is ' +
    formatPercent(1 - data.percent, 2) + '%' + difference;
}

/**
 * Format a number as a percentage
 * @param  {Number} num       Number to format as a percent
 * @param  {Number} precision Precision of the decimal
 * @return {String}           Formatted perentage
 */
function formatPercent(num, precision) {
  return (num * 100).toFixed(precision);
}

/**
 * Delete all files in a given path
 * @param  {Array}   path - array of paths to delete
 * @param  {Function} done - callback when complete
 */
function clean(path, done) {
  log('Cleaning: ' + $.util.colors.blue(path));
  del(path).then(function (paths) {
    done();
  });
}

/**
 * When files change, log it
 * @param  {Object} event - event that fired
 */
function changeEvent(event) {
  var srcPattern = new RegExp('/.*(?=/' + config.source + ')/');
  log('File ' + event.path.replace(srcPattern, '') + ' ' + event.type);
}

/**
 * Format and return the header for files
 * @return {String}           Formatted file header
 */
function getHeader() {
  var pkg = require('./package.json');
  var template = ['/**',
      ' * <%= pkg.name %> - <%= pkg.description %>',
      ' * @authors <%= pkg.authors %>',
      ' * @version v<%= pkg.version %>',
      ' * @link <%= pkg.homepage %>',
      ' * @license <%= pkg.license %>',
      ' */',
      ''
  ].join('\n');
  return $.header(template, {
    pkg: pkg
  });
}

/**
 * Log a message or series of messages using chalk's blue color.
 * Can pass in a string, object or array.
 */
function log(msg) {
  if (typeof(msg) === 'object') {
    for (var item in msg) {
      if (msg.hasOwnProperty(item)) {
        $.util.log($.util.colors.blue(msg[item]));
      }
    }
  } else {
    $.util.log($.util.colors.blue(msg));
  }
}

/**
 * Throw and log an error
 * @param  {String} taskName  Name of the task it was thrown from
 * @param  {String} msg       Message explaining the thrown error
 */
function throwError(taskName, msg) {
  throw new $.util.PluginError({
      plugin: taskName,
      message: msg
    });
}
